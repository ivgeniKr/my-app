﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using University.DAL;
using University.Models.Entities;
using University.Models.Enums;
using University.Models.ViewModels;

namespace University.Services
{
    public class UniversityManager
    {
        private UniversityContext db = new UniversityContext();

        internal List<Person> GetPeopleByCriteria(string searchString, int searchType)
        {
            var people = from m in db.People select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                switch (searchType)
                {
                    case 1: // first name
                        people = people.Where(s => s.FirstName.Contains(searchString));
                        break;
                    case 2: // last name
                        people = people.Where(s => s.LastName.Contains(searchString));
                        break;
                    default:
                        break;
                }
            }

            return people.ToList();
        }

        internal PersonData GetPersonDetailsByTypeAndId(int? id, PersonType type)
        {

            PersonData person = new PersonData();

            switch(type)
            {
                case PersonType.Alumni:
                    person.Alumni = db.Alumnis.Find(id);
                    break;
                case PersonType.Lecturer:
                    person.Lecturer = db.Lecturers.Find(id);
                    break;
                case PersonType.Student:
                    person.Student = db.Students.Find(id);
                    break;
                default:
                    break;
            }

            return person;
        }

        internal bool CreateNewPerson(Person person)
        {

            db.People.Add(person);
            db.SaveChanges();
            return true;

        }

        internal Person GetPersonById(int? id)
        {
            Person person = db.People.Find(id);
            return person;
        }

        internal bool UpdatePersonDetails(PersonData person)
        {
            db.Entry(person).State = EntityState.Modified;
            db.SaveChanges();
            return true;
        }

        internal void DeletePersonById(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
        }

        internal void DisposeDB()
        {
            db.Dispose();
        }
    }
}