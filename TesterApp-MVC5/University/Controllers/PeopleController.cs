﻿
using System.Net;
using System.Web;
using System.Web.Mvc;
using University.Models.Entities;
using University.Models.Enums;
using University.Models.ViewModels;
using University.Services;

namespace University.Controllers
{
    public class PeopleController : Controller
    {
        private readonly UniversityManager _universityManager = new UniversityManager();

        public ActionResult Index(string searchString="", int searchType=0)
        {

            var people = _universityManager.GetPeopleByCriteria(searchString, searchType);
            
            return View(people);

        }

        public ActionResult Details(int? id, PersonType type)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var person = _universityManager.GetPersonDetailsByTypeAndId(id.Value, type);

            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FirstName,LastName,PhoneNumber,Email")] Person person)
        {
            if (ModelState.IsValid)
            {
                var result = _universityManager.CreateNewPerson(person);

                if (result)
                    RedirectToAction("Index");
            }
            
            return View(person);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var person = _universityManager.GetPersonById(id);

            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit(PersonData person)
        {
            if (ModelState.IsValid)
            {
                var res = _universityManager.UpdatePersonDetails(person);
                if (res)
                 return RedirectToAction("Index");
            }
            return View(person);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var person = _universityManager.GetPersonById(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _universityManager.DeletePersonById(id);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _universityManager.DisposeDB();
            }
            base.Dispose(disposing);
        }
    }
}
