﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace University.Models.Enums
{
  public enum PersonType
    {
        Student,
        Alumni,
        Lecturer
    }
}