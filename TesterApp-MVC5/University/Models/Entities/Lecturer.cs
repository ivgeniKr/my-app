﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using University.Models.Enums;

namespace University.Models.Entities
{
    public class Lecturer: Person
    {
        [StringLength(50)]
        public string Subject { get; set; }

        public Lecturer()
        {
            Type = PersonType.Lecturer.ToString();
        }
    }
}