﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using University.Models.Enums;

namespace University.Models.Entities
{
    public class Person
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "First name cannot be longer than 50 characters.")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string Email { get; set; }

        [Required]
        [StringLength(50)]
        public string Type { get; set; }

        public static Type SelectFor(PersonType type)
        {
            switch (type)
            {
                case PersonType.Student:
                    return typeof(Student);
                case PersonType.Alumni:
                    return typeof(Alumni);
                case PersonType.Lecturer:
                    return typeof(Lecturer);
                default:
                    throw new Exception();
            }
        }
    }
}