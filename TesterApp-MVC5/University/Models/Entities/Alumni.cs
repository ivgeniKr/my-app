﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using University.Models.Enums;

namespace University.Models.Entities
{
    public class Alumni: Person
    {
        [Display(Name = "Graduation Year")]
        public int GraduationYear { get; set; }

        public Alumni ()
        {
            Type = PersonType.Alumni.ToString();
        }
    }
}