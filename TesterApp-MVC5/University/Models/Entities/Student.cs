﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using University.Models.Enums;

namespace University.Models.Entities
{
    public class Student: Person
    {
        [StringLength(50)]
        public string Faculty { get; set; }

        public Student()
        {
            Type = PersonType.Student.ToString();
        }
    }
}