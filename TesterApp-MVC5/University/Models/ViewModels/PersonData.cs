﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using University.Models.Entities;

namespace University.Models.ViewModels
{
    public class PersonData
    {
        public Person Person { get; set; }
        public Alumni Alumni { get; set; }
        public Student Student { get; set; }
        public Lecturer Lecturer { get; set; }
    }
}