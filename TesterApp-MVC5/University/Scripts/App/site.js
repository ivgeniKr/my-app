﻿
function Delete(rowId) {
    var check = confirm("Are you sure?");
    if (check === true)
    {
        $.ajax({
            url: "http://localhost:51553/People/Delete",
            type: 'Post',
            data: {
                id: rowId
            },
            dataType: "text/html",
            async: true,
            cache: false,
            sucess: function (data) {
                alert(data);
            },

            error: function (err) {
                alert(err);
            }
        });
    }

    RestoreTbl();
}

function RestoreTbl()
{
    $.ajax({
        url: "http://localhost:51553/People/Index",
        type: 'Get',
        async: true,
        cache: false,
        sucess: function (data) {
            alert(data);
        },
        error: function (err) {
            alert(err);
        }
    });
}
//@Html.ActionLink("Delete", "Delete", new { id=item.ID })