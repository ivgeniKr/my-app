namespace University.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewField_Type : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.People", "Type", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.People", "Type");
        }
    }
}
