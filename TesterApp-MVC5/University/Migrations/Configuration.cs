namespace University.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using University.Models.Entities;

    internal sealed class Configuration : DbMigrationsConfiguration<University.DAL.UniversityContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "University.Models.UniversityContext";
        }

        protected override void Seed(University.DAL.UniversityContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //

            context.People.AddOrUpdate( x => x.ID,
              new Lecturer { FirstName = "Boni", LastName = "Miller", PhoneNumber = "03-3333429", Email = "Boni@gmail.com", Subject = "Math", Type = "Lecturer" },
              new Student { FirstName = "Andrew", LastName = "Peters", PhoneNumber = "03-4294284", Email = "AndrewP@gmail.com", Faculty = "Science", Type= "Student" },
              new Alumni { FirstName = "Brice", LastName = "Lambson", PhoneNumber = "04-4249522", Email ="Brice@gmail.com", GraduationYear= 1992, Type= "Alumni" },
              new Student { FirstName = "Dan", LastName = "Tom", PhoneNumber = "07-635394284", Email = "Dan@gmail.com", Faculty = "Philosophy", Type = "Student" },
              new Lecturer { FirstName = "Rowan", LastName = "Miller", PhoneNumber ="07-42042429", Email = "Rowan@gmail.com", Subject = "Bible", Type= "Lecturer" },
              new Student { FirstName = "Bob", LastName = "Rib", PhoneNumber = "05-2494284", Email = "Bob@gmail.com", Faculty = "Science", Type = "Student" },
              new Alumni { FirstName = "Dilen", LastName = "Dab", PhoneNumber = "01-4149522", Email = "Dilen@gmail.com", GraduationYear = 2002, Type = "Alumni" },
              new Lecturer { FirstName = "Tony", LastName = "Bik", PhoneNumber = "07-420423729", Email = "Tony@gmail.com", Subject = "Literature", Type = "Lecturer" },
              new Student { FirstName = "Jack", LastName = "Tom", PhoneNumber = "07-44294284", Email = "Jack@gmail.com", Faculty = "Philosophy", Type = "Student" }
            );
        }
    }
}
