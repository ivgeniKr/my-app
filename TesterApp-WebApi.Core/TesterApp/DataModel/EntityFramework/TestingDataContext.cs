﻿
using Microsoft.EntityFrameworkCore;
using TesterApp.DomainModel.Entities;

namespace TesterApp.DataModel.EntityFramework
{
    public class TestingDataContext: DbContext
    {
        public TestingDataContext(DbContextOptions<TestingDataContext> options) : base(options)
        {

        }

        public DbSet<Phone> Phones { get; set; }
        public DbSet<Scheduler> Schedulers { get; set; }
        public DbSet<Rule> Rules { get; set; }
    }
}
