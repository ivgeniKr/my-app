﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TesterApp.DomainModel.Services;
using TesterApp.DomainModel.Entities;
using TesterApp.DomainModel.Services.ExceptionHandlers;

namespace TesterApp.Controllers
{
    [Route("api/phones")]
    public class PhonesController : Controller
    {
        private readonly PhoneManager _phoneManager;

        public PhonesController(PhoneManager phoneManager)
        {
            _phoneManager = phoneManager;
        }

        [HttpGet]
        public IEnumerable<Phone> GetAllPhones()
        {
            return _phoneManager.GetAllPhones();
        }


        [HttpGet("{id}", Name = "GetPhone")]
        public IActionResult GetPhoneById(int id)
        {
            var item = _phoneManager.GetPhoneById(id);

            if (item == null)
                return NotFound();

            return new ObjectResult(item);
        }


        [HttpPost]
        [CreateFailureExceptionFilter]
        public IActionResult CreatePhone([FromBody] Phone item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _phoneManager.CreatePhone(item);
               
            return CreatedAtRoute("GetPhone", new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public IActionResult UpdatePhone(int id, [FromBody] Phone item)
        {
            if (item == null || item.Id != id )
                return BadRequest();

            var response = _phoneManager.UpdatePhone(item);

            if (response == null)
                return NotFound();

            return new NoContentResult();

        }

        [HttpDelete("{id}")]
        public IActionResult DeletePhone(int id)
        {
            var response = _phoneManager.DeletePhone(id);

            if (response == null)
                return NotFound();

            return new NoContentResult();
        }


    }
}
