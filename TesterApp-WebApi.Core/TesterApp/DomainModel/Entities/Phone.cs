﻿

namespace TesterApp.DomainModel.Entities
{
    public class Phone
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
    }
}
