﻿

namespace TesterApp.DomainModel.Entities
{
    public class Rule
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
