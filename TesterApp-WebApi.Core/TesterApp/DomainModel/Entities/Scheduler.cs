﻿

namespace TesterApp.DomainModel.Entities
{
    public class Scheduler
    {
        public int Id { get; set; }
        public string JobName { get; set; }
        public string GroupName { get; set; }
        public string TriggerName { get; set; }
        public string JobDescription { get; set; }
        public string CronString { get; set; }

        public int RuleId { get; set; }

        public virtual Rule Rule { get; set; }
    }
}
