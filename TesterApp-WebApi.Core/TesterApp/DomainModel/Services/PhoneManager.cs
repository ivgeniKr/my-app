﻿
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using TesterApp.DataModel.EntityFramework;
using TesterApp.DomainModel.Entities;
using Newtonsoft.Json;

namespace TesterApp.DomainModel.Services
{
    public class PhoneManager
    {
        const string GET_ALL_PHONES = "GetAllPhones";

        private readonly TestingDataContext _context;
        private readonly IDistributedCache _distributedCache;

        public PhoneManager(TestingDataContext context, IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
            _context = context;
            init();
        }

        private void init()
        {
            if (_context.Phones.Count() == 0)
            {
                _context.Phones.Add(new Entities.Phone { Id = 1, Name = "John Bob", Number = "031231234" });
                _context.Phones.Add(new Entities.Phone { Id = 2, Name = "Ronen Kol", Number = "084324423" });
                _context.Phones.Add(new Entities.Phone { Id = 3, Name = "Ruby Tor", Number = "0464334332" });
                _context.SaveChanges();
            }
        }

        internal IEnumerable<Phone> GetAllPhones()
        {
            var phones = GetCachedItem<List<Phone>>(GET_ALL_PHONES);
            if (phones != null)
                return phones;
            else
                CachePhonesList();

            return GetCachedItem<List<Phone>>(GET_ALL_PHONES);

        }

        internal Phone GetPhoneById(int id)
        {
            var item = _context.Phones.SingleOrDefault(t => t.Id == id);           
            return item;
        }

        internal void CreatePhone(Phone item)
        {

            var testItem = _context.Phones.SingleOrDefault(t => t.Id == item.Id);
            if (testItem != null)
                throw new ArgumentException("Already exists");

            _context.Phones.Add(item);
            _context.SaveChanges();

            ReplaceCachedItem(GET_ALL_PHONES);
        }

        internal Phone UpdatePhone(Phone item)
        {
            var oldItem = _context.Phones.SingleOrDefault(t => t.Id == item.Id);
            if (oldItem == null)
                return null;

            oldItem.Name = item.Name;
            oldItem.Number = item.Number;

            _context.Phones.Update(oldItem);
            _context.SaveChanges();

            ReplaceCachedItem(GET_ALL_PHONES);

            return oldItem;

        }

        internal object DeletePhone(int id)
        {
            var item = _context.Phones.SingleOrDefault(t => t.Id == id);
            if (item == null)
                return null;

            _context.Phones.Remove(item);
            _context.SaveChanges();

            ReplaceCachedItem(GET_ALL_PHONES);

            return item;
        }

        // Dont use Redis this way, it was simply meant for redis cache testing.

        private void CachePhonesList()
        {
            List<Phone> phones = _context.Phones.ToList();
            string jsonPhones = JsonConvert.SerializeObject(phones);
            _distributedCache.SetString(GET_ALL_PHONES, jsonPhones);
        }

        private T GetCachedItem <T>(string key)
        {
            string jsonItem = _distributedCache.GetString(key);
            if (jsonItem == null)
                return default(T);
            return JsonConvert.DeserializeObject<T>(jsonItem);
        }

        private void ReplaceCachedItem(string key)
        {
            _distributedCache.Remove(key);
            CachePhonesList();

        }
    }
}
