import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {HomeComponent} from './view-model/home/home.component';
import {SchedulerMainComponent} from './view-model/scheduler/scheduler-main.component';
import {PhoneMainComponent} from './view-model/phone/phone-main.component';

import { LoginComponent } from './view-model/auth/login/login.component';
import { RegisterComponent } from './view-model/auth/register/register.component';
import { AuthGuard } from './shared/security/auth/auth.guard';


@NgModule({
  imports: [
    RouterModule.forRoot([

      { path: '', redirectTo: '/home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent , canActivate: [AuthGuard] },
      { path: 'scheduler', component: SchedulerMainComponent, canActivate: [AuthGuard] },
      { path: 'phoneList', component: PhoneMainComponent, canActivate: [AuthGuard] },
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },

    ])
  ],
  exports: [ RouterModule ]
})

export class RoutingModule {}