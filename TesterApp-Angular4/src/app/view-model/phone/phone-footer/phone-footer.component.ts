import { Component, Input } from '@angular/core';
import { Phone } from '../../../domain-model/entities/phone';

@Component({
  selector: 'phone-footer',
  templateUrl: './phone-footer.component.html',
  styleUrls: ['./phone-footer.component.scss']
})
export class PhoneFooterComponent {

  @Input()
  phones: Phone[];

  constructor() {
  }

}
