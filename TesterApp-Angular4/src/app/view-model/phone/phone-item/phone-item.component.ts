import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Phone } from '../../../domain-model/entities/phone';

@Component({
  selector: 'app-phone-item',
  templateUrl: './phone-item.component.html',
  styleUrls: ['./phone-item.component.scss']
})
export class PhoneItemComponent {

  @Input() phone: Phone;

  @Output()
  remove: EventEmitter<Phone> = new EventEmitter();

  constructor() {
  }

  removePhone(phone: Phone) {
    
    this.remove.emit(phone);
  }

}
