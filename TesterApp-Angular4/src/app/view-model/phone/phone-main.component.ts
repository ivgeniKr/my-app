import { Component, OnInit } from '@angular/core';
import { Phone } from '../../domain-model/entities/phone';
import { PhoneApi} from '../../domain-model/services/phone-api.service'
import { PhoneMock} from '../../domain-model/services/phone-mock.service'

@Component({
  selector: 'phone-main',
  templateUrl: './phone-main.component.html',
  styleUrls: ['./phone-main.component.scss'],
})
export class PhoneMainComponent implements OnInit {

  phones: Phone[] = [];

  constructor(
    private phoneApi: PhoneApi,  // real api
    private phoneMock: PhoneMock // mock data
  ) {
  }

  public ngOnInit() {
   
    this.setPhonesList();
  }

  onAddPhone(phone) {
    
    this.phoneMock
      .createPhone(phone)
      .subscribe(
        (_) => {
          
          this.setPhonesList();
        }
      );
  }

  onRemovePhone(phone) {
    
    this.phoneMock
      .deletePhoneById(phone.id)
      .subscribe(
        (_) => {
          
         this.setPhonesList();
        }
      );
  }

   setPhonesList(){
    this.phoneMock.getAllPhones().subscribe((phones) => { this.phones = phones });
  }
}