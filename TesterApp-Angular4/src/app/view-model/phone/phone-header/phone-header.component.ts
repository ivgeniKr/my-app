import { Component, Output, EventEmitter } from '@angular/core';
import { Phone } from '../../../domain-model/entities/phone';

@Component({
  selector: 'phone-header',
  templateUrl: './phone-header.component.html',
  styleUrls: ['./phone-header.component.scss']
})
export class PhoneHeaderComponent {

  newPhone: Phone = new Phone();

  @Output()
  add: EventEmitter<Phone> = new EventEmitter();

  constructor() {
  }

  addPhone() {
      
    this.add.emit(this.newPhone);
    this.newPhone = new Phone();
  }

}