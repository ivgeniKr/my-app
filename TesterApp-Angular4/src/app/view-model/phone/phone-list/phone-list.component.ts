import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Phone } from '../../../domain-model/entities/phone';

@Component(
  {
    selector: 'app-phone-list',
    templateUrl: './phone-list.component.html',
    styleUrls: ['./phone-list.component.scss']
  }
)
export class PhoneListComponent {

  @Input()
  phones: Phone[];

  @Output()
  remove: EventEmitter<Phone> = new EventEmitter();

  constructor() {
  }

  onRemovePhone(phone: Phone) {
    
    this.remove.emit(phone);
  }

}