import { Component, OnInit } from '@angular/core';
import { MockDataService } from '../../../domain-model/Services/mock-data.service'
import { RuleScheduler } from '../../../domain-model/entities/rule-scheduler'
import { Rules } from '../../../domain-model/entities/rules'
import { FormBuilder,FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'scheduler-create',
  templateUrl: './scheduler-create.component.html',
  styleUrls: ['./scheduler-create.component.scss']
})
export class SchedulerCreateComponent implements OnInit {


public rules : Rules[];
public addSchedulerForm : FormGroup;


  constructor(
    private schedulerService : MockDataService,
    private formBuilder: FormBuilder)  {}

  ngOnInit() {
   
    this.getAllRules();

    this.addSchedulerForm = this.formBuilder.group(
  {
     JobName: ["", Validators.required],
     JobDescription: ["",Validators.required],
     TriggerName: ["", Validators.required],
     CronString: ["", Validators.required],
     RuleId: ["", Validators.required],
     GroupName: ["", Validators.required]
  });

}
 
  private getAllRules()
  {
    
    this.rules = this.schedulerService.getAllRules();
  }

  addNewSchedule()
  {
     
     this.schedulerService.addRuleScheduler(this.addSchedulerForm.value as RuleScheduler);
     this.addSchedulerForm.reset() 
  }

}