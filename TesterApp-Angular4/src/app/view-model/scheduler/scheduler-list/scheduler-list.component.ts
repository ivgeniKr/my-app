import { Component, OnInit, Input } from '@angular/core';
import { MockDataService } from '../../../domain-model/Services/mock-data.service'
import { RuleScheduler } from '../../../domain-model/entities/rule-scheduler'
import { Rules } from '../../../domain-model/entities/rules'
import { FormBuilder,FormGroup, Validators } from '@angular/forms';

import {ConfirmDialogModule,ConfirmationService} from 'primeng/primeng';


@Component({
  selector: 'scheduler-list',
  templateUrl: './scheduler-list.component.html',
  styleUrls: ['./scheduler-list.component.scss']
})
export class SchedulerListComponent implements OnInit {

public schedulerHeaders : string[];
public schedulerRules : RuleScheduler[];
public rules : Rules[];

public visible : boolean = false;

public rulesCreationOrUpdateForm: FormGroup;

//@ViewChild('rulesModal') public rulesModal:ModalDirective;

  constructor(
    private schedulerService : MockDataService,
    private formBuilder: FormBuilder,
    private confirmationService: ConfirmationService)  {}

  ngOnInit() {
    //todo - localize it
    
    //this.schedulerHeaders = ["","תיאור עבודה", "מספר עבודה", "קצב הפעלה", "שם טריגר", "שם קבוצה", "שם עבודה", "מספר מזהה"] 
    this.schedulerHeaders = ["מספר מזהה", "שם עבודה", "שם קבוצה", "שם טריגר", "קצב הפעלה", "מספר עבודה", "תיאור עבודה", ""] 
    
    this.getAllSchedulers();  
    this.getAllRules();

  this.rulesCreationOrUpdateForm = this.formBuilder.group({
      
      Id: ['', [Validators.required]],
      RuleId: ['', [Validators.required]],
      JobName: ['', [Validators.required]],
      JobDescription: ['', [Validators.required]],
      GroupName: ['', [Validators.required]],
      CronString: ['', [Validators.required]],
      TriggerName: ['', [Validators.required]],
    });

}

  cancelModal(){
    //this.rulesModal.hide();
    this.rulesCreationOrUpdateForm.reset();
    this.visible = false;
  }

  OpenEditModal(rule: RuleScheduler){
    this.visible = true;
    //this.rulesModal.show();
    this.rulesCreationOrUpdateForm.patchValue(rule);
    this.rulesCreationOrUpdateForm.get('Id').patchValue(rule.Id);
    this.rulesCreationOrUpdateForm.get('JobName').patchValue(rule.JobName);
    this.rulesCreationOrUpdateForm.get('JobDescription').patchValue(rule.JobDescription);
    this.rulesCreationOrUpdateForm.get('GroupName').patchValue(rule.GroupName);
    this.rulesCreationOrUpdateForm.get('CronString').patchValue(rule.CronString);
    this.rulesCreationOrUpdateForm.get('TriggerName').patchValue(rule.TriggerName);
    this.rulesCreationOrUpdateForm.get('RuleId').patchValue(rule.RuleId);
  }
 

  updateModal(){
    
    this.schedulerService.addRuleScheduler(this.rulesCreationOrUpdateForm.value as RuleScheduler);
    this.cancelModal();
  }

  private getAllRules()
  {
    
    this.rules = this.schedulerService.getAllRules();
  }

  private getAllSchedulers()
  {
    
    this.schedulerRules = this.schedulerService.getAllSchedulers()
  } 

  removeSchedule(rule: RuleScheduler)
  {
    
    this.confirmationService.confirm({
      message:'Are you sure that you want to perform this action?',
            accept: () => {
              this.schedulerService.deleteRuleScheduler(rule);
              this.getAllSchedulers(); }
    });
     
  }

}

