import { Component, OnInit, } from '@angular/core';
import { MockDataService } from '../../domain-model/Services/mock-data.service'
import { RuleScheduler } from '../../domain-model/entities/rule-scheduler'
import { Rules } from '../../domain-model/entities/rules'
import { FormBuilder,FormGroup, Validators } from '@angular/forms';

import {ConfirmDialogModule,ConfirmationService} from 'primeng/primeng';


@Component({
  selector: 'scheduler-main-app',
  templateUrl: './scheduler-main.component.html',
  styleUrls: ['./scheduler-main.component.scss']
})
export class SchedulerMainComponent implements OnInit {


  constructor()  {}

  ngOnInit() {}

}

