import {Injectable} from '@angular/core';
import {RuleScheduler} from '../entities/rule-scheduler';
import {Rules} from '../entities/rules'

@Injectable()
export class MockDataService {

  // Placeholder for last id so we can simulate
  // automatic incrementing of id's
  public lastId: number = 3;
  private rule : Rules;


Id: number;
    JobName: string;
    GroupName: string;
    TriggerName: string;
    JobDescription: string;
    CronString: string;
    RuleId: number;

  public ruleSchedulers: RuleScheduler[] = [
    {Id:1, JobName:"Job1", GroupName:"Group1", TriggerName:"Trigger1", JobDescription:"My Description1", CronString:"01:30", RuleId:1},
    {Id:2, JobName:"Job2", GroupName:"Group2", TriggerName:"Trigger2", JobDescription:"My Description2", CronString:"02:30", RuleId:2},
    {Id:3, JobName:"Job3", GroupName:"Group3", TriggerName:"Trigger3", JobDescription:"My Description3", CronString:"03:30", RuleId:1},
  ];
  public rules: Rules[] = [ {Id: 1, Name: `חוק ראשון` }, {Id: 2, Name:`חוק שני`}];

  constructor() {
    
  }

  
  addRuleScheduler(ruleScheduler: RuleScheduler): MockDataService {
    

   let tRuleScheduler = this.getRuleScheduler(ruleScheduler.JobName, ruleScheduler.GroupName);
    
    
    if (!tRuleScheduler) {
      
      // create
      ruleScheduler.Id = ++this.lastId;
      this.ruleSchedulers.push(ruleScheduler);
    }

    else
    {
      // update
      Object.assign(tRuleScheduler, ruleScheduler);
    }
     
    return this;
  }


  deleteRuleScheduler(ruleScheduler: any): MockDataService {
   
   this.ruleSchedulers = this.ruleSchedulers.filter(item => item.GroupName != ruleScheduler.GroupName || item.JobName != ruleScheduler.JobName);
   return this;
  }

  getAllSchedulers(): RuleScheduler[] {
    
    return this.ruleSchedulers;
  }

  getRuleScheduler(jobName: string, groupName: string): RuleScheduler {
    return this.ruleSchedulers
      .filter(ruleScheduler => ruleScheduler.JobName == jobName && ruleScheduler.GroupName == groupName)
      .pop();
  }

  getAllRules(): Rules[]{
       return this.rules;
  }

  

}