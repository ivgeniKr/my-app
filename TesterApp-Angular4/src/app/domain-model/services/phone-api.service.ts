import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Http, Response } from '@angular/http';
import { Phone } from '../entities/phone';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const API_URL = environment.apiUrl;

@Injectable()
export class PhoneApi {

  constructor(
    private http: Http
  ) {
  }

  public getAllPhones(): Observable<Phone[]> {
    return this.http
      .get(API_URL + '/phones')
      .map(response => {
        const phones = response.json();
        return phones.map((phone) => new Phone(phone));
      })
      .catch(this.handleError);
  }

  public createPhone(phone: Phone): Observable<Phone> {
    return this.http
      .post(API_URL + '/phones', phone)
      .map(response => {
        return new Phone(response.json());
      })
      .catch(this.handleError);
  }

  public getPhoneById(phoneId: number): Observable<Phone> {
    return this.http
      .get(API_URL + '/phones/' + phoneId)
      .map(response => {
        return new Phone(response.json());
      })
      .catch(this.handleError);
  }

  public updatePhone(phone: Phone): Observable<Phone> {
    return this.http
      .put(API_URL + '/phones/' + phone.id, phone)
      .map(response => {
        return new Phone(response.json());
      })
      .catch(this.handleError);
  }

  public deletePhoneById(phoneId: number): Observable<boolean> {
    return this.http
      .delete(API_URL + '/phones/' + phoneId)
      .map(response => null)
      .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    console.error('PhoneApi::handleError', error);
    return Observable.throw(error);
  }
}