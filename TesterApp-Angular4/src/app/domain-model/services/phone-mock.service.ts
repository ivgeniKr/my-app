import { Injectable } from '@angular/core';
import { Phone } from '../entities/phone';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';



@Injectable()
export class PhoneMock {


  nextId: number = 0;

  phones: Phone[] = [
       {id: 1, number: '039223485', name: 'John Smith'},
       {id: 2, number: '039242385', name: 'Bob Smith'} ];;
      
  constructor() {}

  public getAllPhones(): Observable<Phone[]> {
    
    return Observable.of(
      this.phones );
  }

  public createPhone(phone: Phone): Observable<Phone> {
     
    this.setNextIndex()
    phone.id = this.nextId;
    this.phones.push(phone);
    return Observable.of( phone);
  }

  public getPhoneById(phoneId: number): Observable<Phone> {
    let phone = this.phones.filter( t => t.id == phoneId).pop();  
    return Observable.of(
      phone
    );
  }

  public updatePhone(phone: Phone): Observable<Phone> {
    let tPhone = this.getPhoneById(phone.id);
    Object.assign(tPhone, phone);
    return Observable.of(
      phone
    );
  }

  public deletePhoneById(phoneId: number): Observable<boolean> {  
    this.phones = this.phones.filter(phone => phone.id !== phoneId);
    return Observable.of( true);
  }

  private setNextIndex()
  {
      if ( this.phones.length > 0)
        this.nextId = this.phones[this.phones.length-1].id + 1
      else
        this.nextId = 1; 
  }
}
