export class Phone {

    id: number;
    number: string;
    name: string;

    constructor(values: Object = {}){
        Object.assign(this, values);
    }
}