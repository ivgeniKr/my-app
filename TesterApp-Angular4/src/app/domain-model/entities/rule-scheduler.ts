export class RuleScheduler {
    Id: number;
    JobName: string;
    GroupName: string;
    TriggerName: string;
    JobDescription: string;
    CronString: string;
    RuleId: number;

    constructor(values: Object = {}) {
    Object.assign(this, values);
  }

}
