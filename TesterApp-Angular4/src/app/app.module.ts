import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, BaseRequestOptions } from '@angular/http';

import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent} from './view-model/home/home.component'
import { HeaderComponent } from './header/header.component';

import { SchedulerMainComponent } from './view-model/scheduler/scheduler-main.component';
import { SchedulerCreateComponent } from './view-model/scheduler/scheduler-create/scheduler-create.component';
import { SchedulerListComponent } from './view-model/scheduler/scheduler-list/scheduler-list.component';

import { MockDataService } from './domain-model/Services/mock-data.service';

import { RoutingModule} from './routing.module';

import { FirstTabComponent } from './view-model/home/pages/first-tab/first-tab.component';
import { SecondTabComponent } from './view-model/home/pages/second-tab/second-tab.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TabViewModule } from 'primeng/primeng';
import { ConfirmDialogModule,ConfirmationService } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';

import { PhoneMainComponent} from './view-model/phone/phone-main.component';
import { PhoneItemComponent } from './view-model/phone/phone-item/phone-item.component';
import { PhoneFooterComponent } from './view-model/phone/phone-footer/phone-footer.component';
import { PhoneHeaderComponent } from './view-model/phone/phone-header/phone-header.component';
import { PhoneListComponent } from './view-model/phone/phone-list/phone-list.component';
import { PhoneApi } from './domain-model/services/phone-api.service';
import { PhoneMock} from './domain-model/services/phone-mock.service'

import {  AlertService, AuthenticationService, UserService } from './shared/security/services/index';
import { fakeBackendProvider } from './shared/security/helpers/fake-backend';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { AlertComponent } from './shared/security/alert/alert.component';
import { AuthGuard } from './shared/security/auth/auth.guard';
import { LoginComponent } from './view-model/auth/login/login.component';
import { RegisterComponent } from './view-model/auth/register/register.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FirstTabComponent,
    SecondTabComponent,
    SchedulerMainComponent,
    SchedulerListComponent,
    SchedulerCreateComponent,
    PhoneMainComponent,
    PhoneItemComponent,
    PhoneFooterComponent,
    PhoneHeaderComponent,
    PhoneListComponent,
    AlertComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    TabViewModule,
    ConfirmDialogModule,
    DialogModule,
    AccordionModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    RoutingModule
  ],
  providers: [MockDataService,
              ConfirmationService,
              PhoneMock,
              PhoneApi,
              AuthGuard,
              AlertService,
              AuthenticationService,
              UserService,
              fakeBackendProvider,
              MockBackend,
              BaseRequestOptions],
  bootstrap: [AppComponent]
})
export class AppModule { }
